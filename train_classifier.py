from __future__ import print_function
from __future__ import division
import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np
import torchvision
from torchvision import datasets, models, transforms
import matplotlib.pyplot as plt
import time
import os
import copy
from PIL import Image
from tqdm import tqdm
print("PyTorch Version: ",torch.__version__)
model_name = "resnet_rtsd"
batch_size = 64
feature_extract = False
input_size = 16 #224
class_simple = False
class_medium = False
if class_simple:
    num_classes = 8
    num_epochs = 5
elif class_medium:
    num_classes = 115
    num_epochs = 20
else:
    num_classes = 205
    num_epochs = 15



from torch.utils.data import Dataset
class MultiResolutionDataset(Dataset):
    def read_data(self, path):
        import os
        filenames = {}
        filenames_list = []
        classes = {}
        simple_classes = []
        medium_classes = []
        for d in sorted(os.listdir(path)):
            if not os.path.isdir(os.path.join(path, d)):
                continue
            #if len(filenames_list) > 1000:
            #    continue
            classes[d] = len(classes)
            if d.split('.')[0] not in simple_classes:
                simple_classes.append(d.split('.')[0])
            if '.'.join(d.split('.')[:2]) not in medium_classes:
                medium_classes.append('.'.join(d.split('.')[:2]))
            cur_filenames = []
            for file in os.listdir(os.path.join(path, d)):
                if not os.path.isfile(os.path.join(path, d, file)):
                    continue
                cur_filenames.append(os.path.join(path, d, file))
                filenames_list.append((d, os.path.join(path, d, file)))
            filenames[d] = cur_filenames
        simple_classes = {key: val for val, key in enumerate(sorted(simple_classes))}
        medium_classes = {key: val for val, key in enumerate(sorted(medium_classes))}
        return filenames_list, classes, simple_classes, medium_classes
    
    def __init__(self, path_real, path_synt, transform, resolution=input_size, num_classes=205):
        print("Start read real")
        self.data, self.classes, self.simple_classes, self.medium_classes = self.read_data(path_real)
        if path_synt is not None:
            print("Start read synt")
            self.data += self.read_data(path_synt)[0]
        print("End read")
        self.length = len(self.data)
        self.transform = transform
        self.resolution = resolution
        self.num_classes = num_classes
        import random
        random.Random(42).shuffle(self.data)
        print(len(self.classes), len(self.simple_classes), len(self.medium_classes))
        self.class_simple = False
        self.class_medium = False

    def __len__(self):
        return self.length

    def __getitem__(self, index):
        from skimage.io import imread
        from skimage.transform import resize
        import numpy as np
        cls = self.data[index][0]
        img_real = imread(self.data[index][1])
        if img_real.max() > 2:
            img_real = img_real / 255.0
        img_real = resize(img_real, (self.resolution, self.resolution), mode='constant',anti_aliasing=True).astype(np.float32)
        #img_real = Image.fromarray(img_real)
        img_real = self.transform(img_real)
        if self.class_simple:
            return img_real, self.simple_classes[cls.split('.')[0]]
        elif self.class_medium:
            return img_real, self.medium_classes['.'.join(cls.split('.')[:2])]
        else:
            return img_real, self.classes[cls]

    
    
def set_parameter_requires_grad(model, feature_extracting):
    if feature_extracting:
        for param in model.parameters():
            param.requires_grad = False

            
model_ft = models.vgg13_bn(pretrained=True)
model_ft.avgpool = nn.AdaptiveAvgPool2d((2, 2))
model_ft.classifier = nn.Sequential(
    nn.Linear(2048, 512),
    nn.ReLU(True),
    nn.Linear(512, 512),
    nn.ReLU(True),
    nn.Linear(512, num_classes),
)

if input_size == 8:
    model_ft.features = model_ft.features[:-8]
elif input_size == 16:
    model_ft.features = model_ft.features[:-1]


data_transforms = {
    'train': transforms.Compose([
        transforms.ToTensor(),
    ]),
    'val': transforms.Compose([
        transforms.ToTensor(),
    ]),
}


print("Initializing Datasets and Dataloaders...")
path_real = '/mydata/kursovaya4/style-based-gan-pytorch/data/cropped-train/'
path_test = '/mydata/kursovaya4/style-based-gan-pytorch/data/cropped-test/'
path_synt = '/mydata/kursovaya4/style-based-gan-pytorch/data/cropped-synt/'
train_dataset = MultiResolutionDataset(path_real, path_synt, data_transforms['train'], input_size, num_classes)
test_dataset = MultiResolutionDataset(path_test, None, data_transforms['val'], input_size, num_classes)
train_dataset.class_simple = class_simple
test_dataset.class_simple = class_simple
train_dataset.class_medium = class_medium
test_dataset.class_medium = class_medium
# Create training and validation datasets
image_datasets = {'train': train_dataset, 'val': test_dataset}
# Create training and validation dataloaders
dataloaders_dict = {x: torch.utils.data.DataLoader(image_datasets[x], batch_size=batch_size, shuffle=True, num_workers=4) for x in ['train', 'val']}

# Detect if we have a GPU available
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")



# Send the model to GPU
model_ft = model_ft.to(device)
params_to_update = model_ft.parameters()
print("Params to learn:")
if feature_extract:
    params_to_update = []
    for name,param in model_ft.named_parameters():
        if param.requires_grad == True:
            params_to_update.append(param)
            print("\t",name)
else:
    for name,param in model_ft.named_parameters():
        if param.requires_grad == True:
            print("\t",name)

# Observe that all parameters are being optimized
optimizer_ft = optim.Adam(params_to_update, lr=0.001)



def train_model(model, dataloaders, criterion, optimizer, num_epochs=25, is_inception=False):
    since = time.time()

    val_acc_history = []

    best_model_wts = copy.deepcopy(model.state_dict())
    best_acc = 0.0
    
    for epoch in range(num_epochs):
        print('Epoch ' + str(epoch) + '/' + str(num_epochs - 1))
        print('-' * 10)

        # Each epoch has a training and validation phase
        for phase in ['train', 'val']:
            if phase == 'train':
                model.train()  # Set model to training mode
            else:
                model.eval()   # Set model to evaluate mode

            running_loss = 0.0
            running_corrects = 0

            # Iterate over data.
            pbar = tqdm(range(len(dataloaders[phase])))
            for (inputs, labels), pos_bar in zip(dataloaders[phase], pbar):
                inputs = inputs.to(device)
                labels = labels.to(device)

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    # Get model outputs and calculate loss
                    # Special case for inception because in training it has an auxiliary output. In train
                    #   mode we calculate the loss by summing the final output and the auxiliary output
                    #   but in testing we only consider the final output.
                    if is_inception and phase == 'train':
                        # From https://discuss.pytorch.org/t/how-to-optimize-inception-model-with-auxiliary-classifiers/7958
                        outputs, aux_outputs = model(inputs)
                        loss1 = criterion(outputs, labels)
                        loss2 = criterion(aux_outputs, labels)
                        loss = loss1 + 0.4*loss2
                    else:
                        outputs = model(inputs)
                        loss = criterion(outputs, labels)

                    _, preds = torch.max(outputs, 1)

                    # backward + optimize only if in training phase
                    if phase == 'train':
                        loss.backward()
                        optimizer.step()

                # statistics
                running_loss += loss.item() * inputs.size(0)
                running_corrects += torch.sum(preds == labels.data)
                                
                if phase == 'train':
                    epoch_loss = loss.item() / inputs.size(0)
                    epoch_acc = torch.sum(preds == labels.data).double() / inputs.size(0)
                    pbar.set_description('Epoch: ' + str(phase) + ' Loss: ' + str(epoch_loss) + ' Acc: ' + str(float(epoch_acc.data.cpu())))

            pbar.close()
            epoch_loss = running_loss / len(dataloaders[phase].dataset)
            epoch_acc = running_corrects.double() / len(dataloaders[phase].dataset)

            print('Epoch: ' + str(phase) + ' Loss: ' + str(epoch_loss) + ' Acc: ' + str(float(epoch_acc.data.cpu())))

            # deep copy the model
            if phase == 'val' and epoch_acc > best_acc:
                best_acc = epoch_acc
                best_model_wts = copy.deepcopy(model.state_dict())
            if phase == 'val':
                val_acc_history.append(epoch_acc)

        print()

    time_elapsed = time.time() - since
    print('Training complete in ' + str(time_elapsed // 60) + 'm ' + str(time_elapsed % 60) + 's')
    print('Best val Acc: ' + str(best_acc))

    # load best model weights
    model.load_state_dict(best_model_wts)
    return model, val_acc_history



criterion = nn.CrossEntropyLoss()

# Train and evaluate
model_ft, hist = train_model(model_ft, dataloaders_dict, criterion, optimizer_ft, num_epochs=num_epochs, is_inception=False)

torch.save(model_ft.state_dict(), "classify_model_vgg" + str(input_size) + ".pt")