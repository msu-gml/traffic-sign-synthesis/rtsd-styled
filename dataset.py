from torch.utils.data import Dataset
from skimage.transform import rotate
from skimage.io import imread
import random
import json
import random
from skimage.io import imread
from skimage.transform import resize
from skimage.transform import rotate
import numpy as np
import matplotlib.pyplot as plt
from math import sqrt
from skimage.color import rgb2hsv, hsv2rgb
from skimage.filters import gaussian
from random import randrange, seed, uniform
from scipy.signal import convolve2d

def conv(img, k):
    res = []
    for i in range(img.shape[-1]):
        res.append(convolve2d(img[..., i], k, mode='same', boundary='symm'))
    return np.dstack(res)


def expand_image(img):
    n_rows, n_cols, n_channels = img.shape
    new_n_rows = int(n_rows * sqrt(2))
    new_n_cols = int(n_cols * sqrt(2))
    expanded_image = np.full((new_n_rows, new_n_cols, n_channels), 0, dtype=img.dtype)
    expanded_image[..., 3] = 0
    start_row = (new_n_rows - n_rows) // 2
    start_col = (new_n_cols - n_cols) // 2
    expanded_image[start_row:start_row + n_rows, start_col:start_col + n_cols] = img
    return expanded_image


def mul_mask(img, mask):
    res = img.copy()
    res[..., 0] *= mask
    res[..., 1] *= mask
    res[..., 2] *= mask
    return res


def blend_images(icon, bg_img, shift=(0, 0)):
    bg = bg_img.copy()
    mask = icon[..., 3]
    r, c = shift
    n_rows, n_cols, _ = icon.shape
    bg_patch = bg[r:r + n_rows, c:c + n_cols, ...]
    blended_sign = mul_mask(icon[..., :3].astype('float'), mask) + \
        mul_mask(bg_patch.astype('float'), (1 - mask))
    bg[r:r + n_rows, c:c + n_cols] = blended_sign

    full_mask = np.zeros(bg_img.shape[:2], dtype=mask.dtype)
    full_mask[r:r + n_rows, c:c + n_cols] = mask

    return bg.clip(0, 1), full_mask

def change_color(img):
    res = img.copy()
    hsv = rgb2hsv(res[..., :3])

    h_factor = uniform(-0.05, 0.05)
    hsv[...,0] += h_factor
    hsv[..., 0][hsv[..., 0] < 0] += 1
    hsv[..., 0][hsv[..., 0] > 1] -= 1

    s_factor = uniform(0.5, 0.8)
    hsv[..., 1] *= s_factor
    np.clip(hsv[..., 1], 0, 1, out=hsv[..., 1])

    v_factor = uniform(0.5, 0.7)
    hsv[..., 2] *= v_factor
    np.clip(hsv[..., 2], 0, 1, out=hsv[..., 2])

    res[..., :3] = hsv2rgb(hsv)
    return res


def build_motion_kernel(value, angle):
    kernel = np.zeros((value, value))
    kernel[value // 2] = 1
    kernel = rotate(kernel, angle)
    kernel = kernel / kernel.sum()
    return kernel


def augment(bg, sign, center, size, transform=True):
    h, w = sign.shape[:2]
    sign = expand_image(sign)

    if transform:
        sign = change_color(sign)

    if transform:
        angle = uniform(-1, 1)
        sign = rotate(sign, angle)

        value = randrange(1, size // 15 + 1)
        angle = uniform(-90, 90)
        sign = conv(sign, build_motion_kernel(value, angle))

        sigma = 2.5 * (size / 100) / uniform(1, 3)
        sign = gaussian(sign, sigma, multichannel=True)

    n_rows, n_cols = sign.shape[:2]
    c_row, c_col = center
    bg_rows, bg_cols = bg.shape[:2]
    shift_r = max(c_row - n_rows // 2, 0)
    shift_c = max(c_col - n_cols // 2, 0)
    shift_r = min(shift_r, bg_rows - n_rows)
    shift_c = min(shift_c, bg_cols - n_cols)
    shift = (shift_r, shift_c)
    return blend_images(sign, bg, shift=shift)

class IconsImages(object):
    def __init__(self, imgs_path):
        self.imgs, self.imgs_filenames, self.classes, self.int_to_classes = self.read_data(imgs_path)

    def get_int_to_classes(self, index):
        cls = self.int_to_classes[index]
        return cls
    
    def get_classes_to_int(self, cls):
        if cls in self.classes.keys():
            return self.classes[cls]
        return -1
    
    def get_length(self):
        self.length = len(self.imgs)
        return self.length
    
    def read_data(self, path):
        import os
        icons = {}
        classes = {}
        int_to_classes = []
        imgs_filenames = {}
        for d in sorted(os.listdir(path)):
            cls_str = '.'.join(d.split('.')[:-1])
            imgs_filenames[cls_str] = os.path.join(path, d)
            icons[cls_str] = imread(imgs_filenames[cls_str]).astype(np.float32)
            classes[cls_str] = len(classes)
            int_to_classes.append(cls_str)
        return icons, imgs_filenames, classes, int_to_classes
    
    def get_item(self, cls):
        img = self.imgs[self.int_to_classes[cls]]
        if img.max() > 2:
            img = img / 255.0
        return img, self.imgs_filenames[self.int_to_classes[cls]]

class SignImages(object):
    def read_data(self, path):
        import os
        filenames = [{} for i in range(len(self.resolutions))]
        filenames_list = [[] for i in range(len(self.resolutions))]
        for d in sorted(os.listdir(path)):
            for i in range(0, len(self.resolutions)):
                filenames[i][d] = []
            files_list = sorted(os.listdir(os.path.join(path, d)))
            if not self.is_for_train:
                random.Random(52).shuffle(files_list)
                files_list = files_list[:10]
            for file in files_list:
                cur_filename = os.path.join(path, d, file)
                cur_img = imread(cur_filename)
                for i in range(len(self.resolutions)):
                    if self.resolutions[i] <= cur_img.shape[0] or self.resolutions[i] <= cur_img.shape[1]:
                        filenames_list[i].append((d, cur_filename))
                        filenames[i][d].append(cur_filename)
            for i in range(1, len(self.resolutions)):
                if len(filenames[i][d]) <= 2:
                    filenames[i][d] += filenames[i - 1][d]
                    for cur_filename in filenames[i - 1][d]:
                        filenames_list[i].append((d, cur_filename))
            if self.add_icons:
                for i in range(0, len(self.resolutions)):
                    if len(filenames[i][d]) <= 2:
                        filenames[i][d] = [self.icons_dataset.get_item(d)[1]] + filenames[i][d]
            for i in range(1, len(self.resolutions)):
                filenames_list[i] = list(set(filenames_list[i]))
                for d in filenames[i].keys():
                    filenames[i][d] = list(set(filenames[i][d]))
        return filenames, filenames_list
    
    
    def __init__(self, path, transform, icons_dataset, to_encode_resolution, is_for_train=True, is_synt=False, add_icons=False):
        self.resolutions = [8, 16, 32, 64]
        self.resolutions_idxs = {j: i for i, j in enumerate(self.resolutions)}
        self.to_encode_resolution = to_encode_resolution
        self.is_for_train = is_for_train
        self.is_synt = is_synt
        self.add_icons = add_icons
        self.icons_dataset = icons_dataset
        print("Start read")
        self.imgs, self.imgs_list = self.read_data(path)
        print("End read")
        self.transform = transform

    def get_length(self, resolution):
        self.length = len(self.real_imgs_list[self.resolutions_idxs[resolution]]) * 1000
        return self.length
        
    def getitem(self, index, cls, resolution):
        from skimage.io import imread, imsave
        from skimage.transform import resize
        import numpy as np
        cls_str = self.icons_dataset.get_int_to_classes(cls)
        res_ix = self.resolutions_idxs[resolution]

        is_normal_image = True
        if cls_str in self.imgs[res_ix].keys() and len(self.imgs[res_ix][cls_str]) > 0:
            img = imread(self.imgs[res_ix][cls_str][index % len(self.imgs[res_ix][cls_str])])
        else:
            img = imread(self.imgs_list[res_ix][index % len(self.imgs_list[res_ix])][1])
            is_normal_image = False
        if img.max() > 2:
            img = img / 255.0
        img = img[:,:,:3]

        img = resize(img, (resolution, resolution), mode='constant', anti_aliasing=True).astype(np.float32)

        img_to_encode = resize(img, (self.to_encode_resolution, self.to_encode_resolution), mode='constant', anti_aliasing=True).astype(np.float32)
        img = self.transform(img)
        img_to_encode = self.transform(img_to_encode)
        return img, img_to_encode, is_normal_image

class RtsdRoadImages(object):
    def __init__(self,
                 json_path,
                 rtsd_images_dir,
                 resolution=128,
                 context_frac=0.5):
        self.resolution = resolution
        self.rtsd_images_dir = rtsd_images_dir
        self.context_frac = context_frac
        with open(json_path, 'r') as fp:
            json_data_full = json.load(fp)
        self.json_data = []
        self.filenames_to_idxs = []
        fnames = sorted(list(json_data_full.keys()))
        for fname in fnames:
            lst = json_data_full[fname]
            len_start = len(self.json_data)
            self.json_data.extend([(fname, item) for item in lst if (not item['ignore'])])
            self.filenames_to_idxs.append((fname, [i for i in range(len_start, len(self.json_data))]))
        print("rtsdroad", len(self.json_data))
    
    def get_length(self):
        return len(self.json_data)
    
    def get_item_by_bbox(self, bbox_info, fname):
        scene_img = imread(self.rtsd_images_dir + fname)
        max_side = max(bbox_info['h'], bbox_info['w'])
        context_size = int(max_side * self.context_frac) + 1
        context_size_y = context_size + (max_side - bbox_info['h'] + 1) // 2
        context_size_x = context_size + (max_side - bbox_info['w'] + 1) // 2
        
        scene_min_y = max(0, bbox_info['y'] - context_size_y)
        scene_max_y = min(scene_img.shape[0], bbox_info['y'] + bbox_info['h'] + context_size_y)
        scene_min_x = max(0, bbox_info['x'] - context_size_x)
        scene_max_x = min(scene_img.shape[1], bbox_info['x'] + bbox_info['w'] + context_size_x)
                
        paste_from_y = context_size_y - (bbox_info['y'] - scene_min_y)
        paste_to_y = scene_max_y - scene_min_y + paste_from_y
        paste_from_x = context_size_x - (bbox_info['x'] - scene_min_x)
        paste_to_x = scene_max_x - scene_min_x + paste_from_x
        
        to_paste_img = np.zeros((bbox_info['h'] + context_size_y * 2, bbox_info['w'] + context_size_x * 2, 3), dtype=np.float32)
        to_paste_img[paste_from_y:paste_to_y, paste_from_x:paste_to_x] = scene_img[scene_min_y:scene_max_y, scene_min_x:scene_max_x]
        if to_paste_img.max() > 2.0:
            to_paste_img = to_paste_img / 255.0
        pasted_mask = np.zeros((bbox_info['h'] + context_size_y * 2, bbox_info['w'] + context_size_x * 2), dtype=np.float32)
        pasted_mask[context_size_y:context_size_y + bbox_info['h'], context_size_x:context_size_x + bbox_info['w']] = 1.0

        sign_with_area = resize(to_paste_img, (self.resolution, self.resolution), mode='constant', anti_aliasing=True).astype(np.float32)
        sign_mask = resize(pasted_mask, (self.resolution, self.resolution), mode='constant', anti_aliasing=True)
        sign_mask = (sign_mask > 0).astype(np.float32)
        
        new_bbox_info = bbox_info.copy()
        vsp = np.argwhere(sign_mask.sum(axis=1))
        new_bbox_info['y'] = vsp[0][0]
        new_bbox_info['h'] = vsp[-1][0] - vsp[0][0] + 1
        vsp = np.argwhere(sign_mask.sum(axis=0))
        new_bbox_info['x'] = vsp[0][0]
        new_bbox_info['w'] = vsp[-1][0] - vsp[0][0] + 1

        return sign_with_area, sign_mask, bbox_info['sign_class'], new_bbox_info
    
    def get_inpainted_fname(self, bbox_info, fname):
        return fname
        fname_splitted = fname.split('/')
        simple_name = fname_splitted[-1].split('.')
        simple_name[0] = '_'.join(['inpainted', simple_name[0], str(bbox_info['y']), str(bbox_info['h']), str(bbox_info['x']), str(bbox_info['w'])]) + '.png'
        fname_splitted[-1] = '.'.join(simple_name)
        return '/'.join(fname_splitted)
    
    def augment_image_with_icon(self, index, icon, randgen):
        item = self.json_data[index % len(self.json_data)]
        fname = item[0]
        bbox_info = item[1]
        koef = randgen.uniform(0.8, 1.3)
        bbox_info['y'] -= int(bbox_info['h'] * (koef - 1.0) // 2)
        bbox_info['h'] += int(bbox_info['h'] * (koef - 1.0))
        bbox_info['x'] -= int(bbox_info['w'] * (koef - 1.0) // 2)
        bbox_info['w'] += int(bbox_info['w'] * (koef - 1.0))
        cropped_for_inpainting = self.get_item_by_bbox(bbox_info, self.get_inpainted_fname(bbox_info, fname))[0]

        
        shape_distort = (int(icon.shape[0] * randgen.uniform(0.9, 1.1)), int(icon.shape[1] * randgen.uniform(0.9, 1.1)))
        icon = resize(icon, shape_distort, mode='constant', anti_aliasing=True).astype(np.float32)
        side = int(self.resolution * randgen.uniform(0.40, 0.45))   #0.44
        if icon.shape[0] > icon.shape[1]:
            new_icon_shape = (side, int(side * icon.shape[1] / icon.shape[0]))
        else:
            new_icon_shape = (int(side * icon.shape[0] / icon.shape[1]), side)
        icon = resize(icon, new_icon_shape, mode='constant', anti_aliasing=True).astype(np.float32)
        
        r_center = cropped_for_inpainting.shape[0] // 2
        c_center = cropped_for_inpainting.shape[1] // 2
        synt_img, full_synt_mask = augment(cropped_for_inpainting, icon, (r_center, c_center), 64)
        return cropped_for_inpainting, synt_img, full_synt_mask

    def get_real_item(self, index):
        item = self.json_data[index % len(self.json_data)]
        fname = item[0]
        bbox_info = item[1]
        return self.get_item_by_bbox(bbox_info, fname)[0]

    
    
class InpaitedCropped(object):
    def __init__(self, imgs_dir, resolution=128, rtsd_road_json=None):
        import os
        self.imgs_fnames = []
        for d in sorted(os.listdir(imgs_dir)):
            self.imgs_fnames.append(os.path.join(imgs_dir, d))
        self.resolution = resolution
        if rtsd_road_json is not None:
            with open(rtsd_road_json, 'r') as fp:
                rtsd_road_json = json.load(fp)
            keys = set(sorted(list(rtsd_road_json.keys())))
            self.imgs_fnames = [elem for elem in self.imgs_fnames if '_'.join(elem.split('/')[-1].split('_')[1:][:-4]) + '.jpg' in keys]
        print("inpcropped", len(self.imgs_fnames))
    def augment_image_with_icon(self, index, icon, randgen, is_test=False):
        fname = self.imgs_fnames[index % len(self.imgs_fnames)]
        bbox_info = fname[:-4].split('_')[-4:]
        bbox_info = {
            "y": int(bbox_info[0]),
            "h": int(bbox_info[1]),
            "x": int(bbox_info[2]),
            "w": int(bbox_info[3]),
            "fname": '_'.join(('_'.join(fname.split('_')[:-4]) + ".jpg").split('/')[-1].split('_')[1:])
        }
        cropped_for_inpainting = imread(self.imgs_fnames[index % len(self.imgs_fnames)])
        if cropped_for_inpainting.max() > 2.0:
            cropped_for_inpainting = cropped_for_inpainting / 255.0
        if is_test:
            side = int(self.resolution * 0.4)
        else:
            side = int(self.resolution * randgen.uniform(0.40, 0.45)) # 0.44
        if icon.shape[0] > icon.shape[1]:
            new_icon_shape = (side, int(side * icon.shape[1] / icon.shape[0]))
        else:
            new_icon_shape = (int(side * icon.shape[0] / icon.shape[1]), side)
        icon = resize(icon, new_icon_shape, mode='constant', anti_aliasing=True).astype(np.float32)
        
        r_center = cropped_for_inpainting.shape[0] // 2
        c_center = cropped_for_inpainting.shape[1] // 2
        synt_img, full_synt_mask = augment(cropped_for_inpainting, icon, (r_center, c_center), 64)
        return cropped_for_inpainting, synt_img, full_synt_mask, bbox_info
    

def xisx(x):
    return x
    
class RtsdDataset(object):
    def __init__(self, crop_size, to_encode_resolution=64, is_for_train=True,
                 icons_signs_path=None, real_signs_path=None, rtsd_road_path=None,
                 rtsd_road_json=None, inpainted_cropped_path=None):
        super(RtsdDataset, self).__init__()
        self.icons_dataset = IconsImages(icons_signs_path)
        self.real_signs = SignImages(real_signs_path, xisx, self.icons_dataset, to_encode_resolution, is_for_train, False, False)
        self.rtsd_road = RtsdRoadImages(rtsd_road_json, rtsd_road_path, crop_size)
        self.inpainted_road = InpaitedCropped(inpainted_cropped_path, crop_size, rtsd_road_json)        
        self.resolution = 64
        self.to_encode_resolution = to_encode_resolution
        self.crop_size = crop_size
        import gc
        gc.collect()
        
        
    def __len__(self):
        return self.rtsd_road.get_length()
    
    def __getitem__(self, index, rand):
        cls = index % 205
        icon = self.icons_dataset.get_item(cls)[0]
        img_real, _, is_normal_real = self.real_signs.getitem(index + rand.randint(0,100000), cls, self.resolution)
        inpainted, pasted_synt, synt_mask, _ = self.inpainted_road.augment_image_with_icon(index, icon, rand)
        sign_synt = pasted_synt[self.crop_size // 4:- self.crop_size // 4, self.crop_size // 4:- self.crop_size // 4]
        sign_synt_mask = synt_mask[self.crop_size // 4: - self.crop_size // 4, self.crop_size // 4:- self.crop_size // 4]
        
        sign_synt_to_encode = resize(sign_synt, (self.to_encode_resolution, self.to_encode_resolution), mode='constant', anti_aliasing=True).astype(np.float32)
        sign_synt = resize(sign_synt, (self.resolution, self.resolution), mode='constant', anti_aliasing=True).astype(np.float32)
        sign_synt_mask = resize(sign_synt_mask, (self.resolution, self.resolution), mode='constant', anti_aliasing=True).astype(np.float32)
        
        inpainted_to_encode = resize(inpainted, (self.to_encode_resolution, self.to_encode_resolution), mode='constant', anti_aliasing=True).astype(np.float32)
        inpainted = resize(inpainted, (self.resolution * 2, self.resolution * 2), mode='constant', anti_aliasing=True).astype(np.float32)

        real_not_inpainted = self.rtsd_road.get_real_item(index + rand.randint(0,100000))
        real_not_inpainted = resize(real_not_inpainted, (self.resolution * 2, self.resolution * 2), mode='constant', anti_aliasing=True).astype(np.float32)
        
        return img_real, is_normal_real, sign_synt, sign_synt_mask, sign_synt_to_encode, \
               inpainted, inpainted_to_encode, real_not_inpainted, \
               self.icons_dataset.get_int_to_classes(cls), cls

    
    def get_for_test(self, cls, index, rand):
        cls = cls % 205
        icon = self.icons_dataset.get_item(cls)[0]
        #inpainted, pasted_synt, synt_mask = self.rtsd_road.augment_image_with_icon(index, icon, rand)
        inpainted, pasted_synt, synt_mask, bbox_info = self.inpainted_road.augment_image_with_icon(index, icon, rand, is_test=True)

        sign_synt = pasted_synt[self.crop_size // 4:- self.crop_size // 4, self.crop_size // 4:- self.crop_size // 4]
        sign_synt_mask = synt_mask[self.crop_size // 4: - self.crop_size // 4, self.crop_size // 4:- self.crop_size // 4]
        
        sign_synt_to_encode = resize(sign_synt, (self.to_encode_resolution, self.to_encode_resolution), mode='constant', anti_aliasing=True).astype(np.float32)
        sign_synt_mask = resize(sign_synt_mask, (self.resolution, self.resolution), mode='constant', anti_aliasing=True).astype(np.float32)
        
        inpainted_to_encode = resize(inpainted, (self.to_encode_resolution, self.to_encode_resolution), mode='constant', anti_aliasing=True).astype(np.float32)
        inpainted = resize(inpainted, (self.resolution * 2, self.resolution * 2), mode='constant', anti_aliasing=True).astype(np.float32)

        icon = resize(icon, (self.resolution * 2, self.resolution * 2), mode='constant', anti_aliasing=True)[:,:,:3].astype(np.float32)
        
        return icon, sign_synt_to_encode, inpainted_to_encode, inpainted, sign_synt_mask, pasted_synt, bbox_info

class MultiResolutionDataset(Dataset):
    def __init__(self, path_real, transform, resolution=128, is_for_train=True,
                 icons_signs_path=None, real_signs_path=None, rtsd_road_path=None,
                 rtsd_road_json=None, inpainted_cropped_path=None):
        self.rtsd_dataset = RtsdDataset(128, is_for_train=is_for_train,
            icons_signs_path=icons_signs_path, real_signs_path=real_signs_path,
            rtsd_road_path=rtsd_road_path, rtsd_road_json=rtsd_road_json,
            inpainted_cropped_path=inpainted_cropped_path)
        self.resolution = resolution
        self.transform = transform
        self.is_first = True
        
    def __len__(self):
        return len(self.rtsd_dataset)
    
    def __getitem__(self, index):
        if self.is_first:
            import random
            self.rnd = random.Random(index)
            self.is_first = False        
        self.rtsd_dataset.resolution = self.resolution
        img_real, is_normal_real, sign_synt, sign_synt_mask, sign_synt_to_encode, \
               inpainted, inpainted_to_encode, real_not_inpainted, \
               cls_str, cls = self.rtsd_dataset.__getitem__(index, self.rnd)
        
        img_real = self.transform(img_real)
        sign_synt = self.transform(sign_synt)
        sign_synt_to_encode = self.transform(sign_synt_to_encode)
        sign_synt_mask = self.transform(sign_synt_mask[:,:,None])
        inpainted = self.transform(inpainted)
        inpainted_to_encode = self.transform(inpainted_to_encode)
        real_not_inpainted = self.transform(real_not_inpainted)
        return img_real, is_normal_real, sign_synt_to_encode, sign_synt, sign_synt_mask, inpainted, inpainted_to_encode, real_not_inpainted, cls
    
    def get_for_test(self, cls, index, is_do_trainsform=True):
        if self.is_first:
            import random
            self.rnd = random.Random(index)
            self.is_first = False
        self.rtsd_dataset.resolution = self.resolution
        img_synt_icon, img_synt, inpainted_to_encode, inpainted, sign_synt_mask, pasted_synt, bbox_info = self.rtsd_dataset.get_for_test(cls, index, self.rnd)
        
        if is_do_trainsform:
            img_synt_icon = self.transform(img_synt_icon)
            img_synt = self.transform(img_synt)
            inpainted_to_encode = self.transform(inpainted_to_encode)
            inpainted = self.transform(inpainted)
            sign_synt_mask = self.transform(sign_synt_mask)
            pasted_synt = self.transform(pasted_synt)
        return img_synt_icon, img_synt, inpainted_to_encode, inpainted, sign_synt_mask, pasted_synt, bbox_info

    

class PicklableMultiResolutionDataset(object):
    def __init__(self, path_real, transform, resolution=128, is_for_train=True,
                 icons_signs_path=None, real_signs_path=None, rtsd_road_path=None,
                 rtsd_road_json=None, inpainted_cropped_path=None):
        self.rtsd_dataset = RtsdDataset(128, is_for_train=is_for_train,
            icons_signs_path=icons_signs_path, real_signs_path=real_signs_path,
            rtsd_road_path=rtsd_road_path, rtsd_road_json=rtsd_road_json,
            inpainted_cropped_path=inpainted_cropped_path)
        self.resolution = resolution
        self.transform = transform
        self.is_first = True
        
    def __len__(self):
        return len(self.rtsd_dataset)
    
    def __getitem__(self, index):
        if self.is_first:
            import random
            self.rnd = random.Random(index)
            self.is_first = False        
        self.rtsd_dataset.resolution = self.resolution
        img_real, is_normal_real, sign_synt, sign_synt_mask, sign_synt_to_encode, \
               inpainted, inpainted_to_encode, real_not_inpainted, \
               cls_str, cls = self.rtsd_dataset.__getitem__(index, self.rnd)
        
        img_real = self.transform(img_real)
        sign_synt = self.transform(sign_synt)
        sign_synt_to_encode = self.transform(sign_synt_to_encode)
        sign_synt_mask = self.transform(sign_synt_mask[:,:,None])
        inpainted = self.transform(inpainted)
        inpainted_to_encode = self.transform(inpainted_to_encode)
        real_not_inpainted = self.transform(real_not_inpainted)
        return img_real, is_normal_real, sign_synt_to_encode, sign_synt, sign_synt_mask, inpainted, inpainted_to_encode, real_not_inpainted, cls
    
    def get_for_test(self, cls, index, is_do_trainsform=True):
        if self.is_first:
            import random
            self.rnd = random.Random(index)
            self.is_first = False
        self.rtsd_dataset.resolution = self.resolution
        _, img_synt, inpainted_to_encode, inpainted, sign_synt_mask, _, bbox_info = self.rtsd_dataset.get_for_test(cls, index, self.rnd)
        
        if is_do_trainsform:
            img_synt = img_synt.transpose((2,0,1))
            inpainted_to_encode = inpainted_to_encode.transpose((2,0,1))
            inpainted = inpainted.transpose((2,0,1))
            sign_synt_mask = sign_synt_mask[None,...]
        return img_synt, inpainted_to_encode, inpainted, sign_synt_mask, None, bbox_info
