import argparse
import random
import math

from tqdm import tqdm
import numpy as np
from PIL import Image

import torch
from torch import nn, optim
from torch.nn import functional as F
from torch.autograd import Variable, grad
from torch.utils.data import DataLoader
from torchvision import datasets, transforms, utils

from dataset import MultiResolutionDataset
from model import StyledGenerator, Discriminator


def requires_grad(model, flag=True):
    for p in model.parameters():
        p.requires_grad = flag


def accumulate(model1, model2, decay=0.999):
    par1 = dict(model1.named_parameters())
    par2 = dict(model2.named_parameters())

    for k in par1.keys():
        par1[k].data.mul_(decay).add_(1 - decay, par2[k].data)


def sample_data(dataset, batch_size, image_size=4, class_simple=False, class_medium=False):
    dataset.resolution = image_size
    dataset.class_simple = class_simple
    dataset.class_medium = class_medium
    loader = DataLoader(dataset, shuffle=True, batch_size=batch_size, num_workers=8, drop_last=True)

    return loader


def adjust_lr(optimizer, lr):
    for group in optimizer.param_groups:
        mult = group.get('mult', 1)
        group['lr'] = lr * mult


def train(args, dataset, generator, discriminator, used_sample=0, step=None, i=0, this_step_iters=0):
    if step is None:
        step = int(math.log2(args.init_size)) - 2
    if i is None:
        i = 0
    if used_sample is None:
        used_sample = 0
    
    resolution = 4 * 2 ** step
    loader = sample_data(
        dataset, args.batch.get(resolution, args.batch_default), resolution, step == 1, step == 2
    )
    data_loader = iter(loader)

    adjust_lr(g_optimizer, args.lr.get(resolution, 0.0005))
    adjust_lr(d_optimizer, args.lr.get(resolution, 0.0005))

    pbar = tqdm(range(i, 3000000))

    requires_grad(generator, False)
    requires_grad(discriminator, True)

    disc_loss_val = 0
    gen_loss_val = 0
    grad_loss_val = 0

    alpha = 0

    max_step = int(math.log2(args.max_size)) - 2
    final_progress = False
    #this_step_iters = 0

    for i in pbar:
        discriminator.zero_grad()

        alpha = min(1, 1 / args.phase * (used_sample + 1))

        if (resolution == args.init_size and args.ckpt is None) or final_progress:
            alpha = 1

        if used_sample > args.phase * 2:
            used_sample = 0
            step += 1
            this_step_iters = 0

            if step > max_step:
                step = max_step
                final_progress = True
                ckpt_step = step + 1

            else:
                alpha = 0
                ckpt_step = step

            resolution = 4 * 2 ** step

            loader = sample_data(
                dataset, args.batch.get(resolution, args.batch_default), resolution, step == 1, step == 2
            )
            data_loader = iter(loader)

            torch.save(
                {
                    'generator': generator.module.state_dict(),
                    'discriminator': discriminator.module.state_dict(),
                    'g_optimizer': g_optimizer.state_dict(),
                    'd_optimizer': d_optimizer.state_dict(),
                    'g_running': g_running.state_dict(),
                    'used_sample': used_sample,
                    'this_step_iters': this_step_iters,
                    'step': step,
                    'i': i,
                },
                args.checkpoint_path + '/train_step-' + str(ckpt_step) + '.model',
            )

            adjust_lr(g_optimizer, args.lr.get(resolution, 0.0005))
            adjust_lr(d_optimizer, args.lr.get(resolution, 0.0005))

        try:
            real_image, is_normal_real, synt_image, img_synt_example, sign_synt_mask, inpainted, inpainted_to_encode, real_not_inpainted, cls = next(data_loader)

        except (OSError, StopIteration):
            data_loader = iter(loader)
            real_image, is_normal_real, synt_image, img_synt_example, sign_synt_mask, inpainted, inpainted_to_encode, real_not_inpainted, cls = next(data_loader)

        used_sample += real_image.shape[0]
        this_step_iters += 1

        b_size = real_image.size(0)
        real_image = real_image.cuda()
        synt_image = synt_image.cuda()
        img_synt_example = img_synt_example.cuda()
        inpainted_to_encode = inpainted_to_encode.cuda()
        real_not_inpainted = real_not_inpainted.cuda()
        sign_synt_mask = sign_synt_mask.cuda()
        inpainted = inpainted.cuda()
        is_normal_real = is_normal_real.cuda().float()

        if args.loss == 'wgan-gp':
            real_predict = discriminator(real_image, real_not_inpainted, step=step, alpha=alpha, i_step=i)
            real_predict = real_predict.mean() - 0.001 * (real_predict ** 2).mean()
            (-real_predict).backward()

            
        elif args.loss == 'r1':
            real_image.requires_grad = True
            real_scores = discriminator(real_image, real_not_inpainted, step=step, alpha=alpha, i_step=i)
            real_predict = F.softplus(-real_scores).mean()
            real_predict.backward(retain_graph=True)

            grad_real = grad(
                outputs=real_scores.sum(), inputs=real_image, create_graph=True
            )[0]
            grad_penalty = (
                grad_real.view(grad_real.size(0), -1).norm(2, dim=1) ** 2
            ).mean()
            grad_penalty = 10 / 2 * grad_penalty
            grad_penalty.backward()
            if i%10 == 0:
                grad_loss_val = grad_penalty.item()

                
                
        if args.mixing and random.random() < 0.9:
            gen_in11, gen_in12, gen_in21, gen_in22 = torch.randn(
                4, b_size, code_size, device='cuda'
            ).chunk(4, 0)
            gen_in1 = [gen_in11.squeeze(0), gen_in12.squeeze(0)]
            gen_in2 = [gen_in21.squeeze(0), gen_in22.squeeze(0)]

        else:
            gen_in1, gen_in2 = torch.randn(2, b_size, code_size, device='cuda').chunk(
                2, 0
            )
            gen_in1 = gen_in1.squeeze(0)
            gen_in2 = gen_in2.squeeze(0)

            
        #from skimage.io import imread, imsave
        #imsave("/mydata/kursovaya4/style-based-gan-pytorch/checking/synt1_" + str(i) + ".png", np.array(synt_image.data.cpu())[0].transpose((1,2,0)))
            
        fake_image = generator(synt_image, gen_in1, inpainted_to_encode, step=step, alpha=alpha)[0]
        #fake_image, cls_loss, identity_loss = generator(synt_image, gen_in1, step=step, alpha=alpha, cls=cls)
        y_from = inpainted.shape[2] // 4
        h_fake = fake_image.shape[2]
        x_from = inpainted.shape[3] // 4
        w_fake = fake_image.shape[3]
        inpainted_pasted_sign = inpainted.clone().detach()
        inpainted_pasted_sign[:,:,y_from:y_from + h_fake, x_from:x_from + w_fake] = fake_image * sign_synt_mask + inpainted_pasted_sign[:,:,y_from:y_from + h_fake, x_from:x_from + w_fake] * (1.0 - sign_synt_mask)
        
        
        '''
        print(np.array(inpainted.data.cpu()).max(), np.array(fake_image.data.cpu()).max(), np.array(sign_synt_mask.data.cpu()).max())
        
        from skimage.io import imread, imsave
        imsave("./checking/synt0_" + str(i) + ".png", np.array(inpainted.data.cpu())[0].transpose((1,2,0)))
        imsave("./checking/synt1_" + str(i) + ".png", np.array(inpainted_pasted_sign.data.cpu())[0].transpose((1,2,0)))
        imsave("./checking/synt2_" + str(i) + ".png", np.array(sign_synt_mask.data.cpu())[0].transpose((1,2,0))[:,:,0])
        imsave("./checking/synt3_" + str(i) + ".png", np.array(fake_image.data.cpu())[0].transpose((1,2,0)))
        imsave("./checking/synt4_" + str(i) + ".png", np.array(real_image.data.cpu())[0].transpose((1,2,0)))
        imsave("./checking/synt5_" + str(i) + ".png", np.array(real_not_inpainted.data.cpu())[0].transpose((1,2,0)))
        '''

        fake_predict = discriminator(fake_image, inpainted_pasted_sign, step=step, alpha=alpha, i_step=i)
        
        if args.loss == 'wgan-gp':
            fake_predict = fake_predict.mean()
            fake_predict.backward()

            eps = torch.rand(b_size, 1, 1, 1).cuda()
            x_hat = eps * real_image.data + (1 - eps) * fake_image.data
            x_hat.requires_grad = True
            inpainted_hat = eps * real_not_inpainted.data + (1 - eps) * inpainted_pasted_sign.data
            #inpainted_hat = inpainted_pasted_sign.data
            inpainted_hat.requires_grad = True
            hat_predict = discriminator(x_hat, inpainted_hat, step=step, alpha=alpha, i_step=i)

            grad_x_hat = grad(
                outputs=hat_predict.sum(), inputs=[x_hat, inpainted_hat], create_graph=True , allow_unused=True
            )
            
            grad_penalty = (
                (grad_x_hat[0].view(grad_x_hat[0].size(0), -1).norm(2, dim=1) - 1) ** 2
            ).mean() + (
                (grad_x_hat[1].view(grad_x_hat[1].size(0), -1).norm(2, dim=1) - 1) ** 2
            ).mean()
            grad_penalty = 10 * grad_penalty
            grad_penalty.backward()
            if i%10 == 0:
                grad_loss_val = grad_penalty.item()
                disc_loss_val = (-real_predict + fake_predict).item()

        elif args.loss == 'r1':
            fake_predict = F.softplus(fake_predict).mean()
            fake_predict.backward()
            if i%10 == 0:
                disc_loss_val = (real_predict + fake_predict).item()

        nn.utils.clip_grad_norm_(discriminator.parameters(), 0.5)
        d_optimizer.step()
        #for name,param in generator.named_parameters():
        #    print(name)

        if (i + 1) % n_critic == 0:
            generator.zero_grad()

            requires_grad(generator.module.generator, True)
            requires_grad(generator.module.synt_img_style, True)
            requires_grad(generator.module.synt_img_style_c, True)
            
            requires_grad(generator.module.classifier_on_style_features255, True)
            requires_grad(discriminator, False)
            

            fake_image, cls_loss = generator(synt_image, gen_in2, inpainted_to_encode, step=step, alpha=alpha, cls=cls, real_cls=cls)
            
            y_from = inpainted.shape[2] // 4
            h_fake = fake_image.shape[2]
            x_from = inpainted.shape[3] // 4
            w_fake = fake_image.shape[3]
            inpainted_pasted_sign = inpainted.clone().detach()
            inpainted_pasted_sign[:,:,y_from:y_from + h_fake, x_from:x_from + w_fake] = fake_image * sign_synt_mask + inpainted_pasted_sign[:,:,y_from:y_from + h_fake, x_from:x_from + w_fake] * (1.0 - sign_synt_mask)

            predict = discriminator(fake_image, inpainted_pasted_sign, step=step, alpha=alpha, i_step=i)

            if args.loss == 'wgan-gp':
                loss = -predict.mean()

            elif args.loss == 'r1':
                loss = F.softplus(-predict).mean()

                
            if args.vgg_loss and step < len(generator.module.vggloss_models) and generator.module.vggloss_models[step] is not None:
                vgg_loss = generator.module.vggloss_models[step](fake_image, img_synt_example, synt_imgs_loss=True)
                vgg_loss += generator.module.vggloss_models[step](fake_image, inpainted[:,:,y_from:y_from + h_fake, x_from:x_from + w_fake], synt_imgs_loss=True) * 0.4 # 0.3
                vgg_loss = 2.5 * vgg_loss
            else:
                vgg_loss = 0
                
            if args.cls_loss:
                loss += 1.8 * cls_loss
            if args.vgg_loss:
                loss += vgg_loss

            if i%10 == 0:
                gen_loss_val = loss.item()

            loss.backward()
            nn.utils.clip_grad_norm_(generator.parameters(), 0.5)
            g_optimizer.step()
            accumulate(g_running, generator.module)

            requires_grad(generator, False)
            requires_grad(discriminator, True)

        if (i + 1) % 200 == 0:
            images = []
            cls_es = []

            gen_i, gen_j = args.gen_sample.get(resolution, (10, 5))

            with torch.no_grad():
                for _ in range(gen_i):
                    try:
                        _, _, synt_image, _, sign_synt_mask, inpainted, inpainted_to_encode, _, cls = next(data_loader)
                    except (OSError, StopIteration):
                        data_loader = iter(loader)
                        _, _, synt_image, _, sign_synt_mask, inpainted, inpainted_to_encode, _, cls = next(data_loader)
                    synt_image = synt_image.cuda()[:gen_j]
                    inpainted_to_encode = inpainted_to_encode.cuda()[:gen_j]
                    sign_synt_mask = sign_synt_mask.cuda()[:gen_j]
                    inpainted = inpainted.cuda()[:gen_j]
                    cls = cls[:gen_j]
                    
                    fake_image = g_running(synt_image, torch.randn(gen_j, code_size).cuda(), inpainted_to_encode, step=step, alpha=alpha)
                    y_from = inpainted.shape[2] // 4
                    h_fake = fake_image.shape[2]
                    x_from = inpainted.shape[3] // 4
                    w_fake = fake_image.shape[3]
                    inpainted[:,:,y_from:y_from + h_fake, x_from:x_from + w_fake] = fake_image * sign_synt_mask + inpainted[:,:,y_from:y_from + h_fake, x_from:x_from + w_fake] * (1.0 - sign_synt_mask)

                    images.append(inpainted.data.cpu())
                    cls_es.extend(map(str, map(int, list(cls.data.cpu()))))

            utils.save_image(
                torch.cat(images, 0),
                args.sample_path + '/' + str(i + 1).zfill(6) + '_' + '_'.join(cls_es)+ '.png',
                nrow=gen_i,
                normalize=False, #True,
                #range=(-1, 1),
            )
            

        if (i + 1) % 1000 == 0:
            torch.save(
                {
                    'generator': generator.module.state_dict(),
                    'discriminator': discriminator.module.state_dict(),
                    'g_optimizer': g_optimizer.state_dict(),
                    'd_optimizer': d_optimizer.state_dict(),
                    'g_running': g_running.state_dict(),
                    'used_sample': used_sample,
                    'this_step_iters': this_step_iters,
                    'step': step,
                    'i': i,
                },
                args.checkpoint_path + '/' + str(i + 1).zfill(6) + '.model',
            )

        state_msg = (
            'Size: ' + str(4 * 2 ** step) + '; G: ' + '%.3f'%(gen_loss_val) + '; D: ' + '%.3f'%(disc_loss_val) + ';' + 
            ' Grad: ' + '%.3f'%(grad_loss_val) + '; Alpha: ' + '%.5f'%(alpha)
        )
        if cls_loss:
            state_msg += '; cls_los: ' + '%.3f'%(cls_loss.item())
            try:
                state_msg += '; vgg_loss: ' + '%.3f'%(vgg_loss.item())
            except:
                state_msg += '; vgg_loss: ' + '%.3f'%(vgg_loss)

        pbar.set_description(state_msg)


if __name__ == '__main__':
    #code_size = 512
    batch_size = 16
    n_critic = 1

    parser = argparse.ArgumentParser(description='Progressive Growing of GANs')

    parser.add_argument('--path_real', type=str, default='/mydata/kursovaya4/style-based-gan-pytorch/data/cropped-train/', help='path of target dataset')
    parser.add_argument('--checkpoint_path', type=str, default='./checkpoint/', help='path of synt dataset')
    parser.add_argument('--icons_signs_path', type=str, default='/mydata/kursovaya4/style-based-gan-pytorch/data/icons/')
    parser.add_argument('--real_signs_path', type=str, default='/mydata/kursovaya4/style-based-gan-pytorch/data/cropped-train/')
    parser.add_argument('--rtsd_road_path', type=str, default='/mydata/kursovaya4/BicycleGAN/data_rtsd/detection_data/')
    parser.add_argument('--rtsd_road_json', type=str, default='/mydata/kursovaya4/BicycleGAN/data_rtsd/train_rtsd.json')
    parser.add_argument('--inpainted_cropped_path', type=str, default='/mydata/kursovaya4/edge-connect/results_better3/')

    parser.add_argument('--sample_path', type=str, default='./sample/', help='path of synt dataset')
    parser.add_argument('--cls_loss', action='store_true', help='use lr scheduling')
    parser.add_argument('--prefix_path_to_vgg_models', type=str, default='/mydata/kursovaya4/style-based-gan-pytorch/classify_model_vgg', help='path for prefixes for vgg model')
    parser.add_argument('--vgg_loss', action='store_true', help='use lr scheduling')
    parser.add_argument(
        '--phase',
        type=int,
        default=100000,
        help='number of samples used for each training phases',
    )
    parser.add_argument('--lr', default=0.03, type=float, help='learning rate')
    parser.add_argument('--sched', action='store_true', help='use lr scheduling')
    parser.add_argument('--init_size', default=8, type=int, help='initial image size')
    parser.add_argument('--code_size', default=612, type=int, help='initial image size')
    parser.add_argument('--max_size', default=64, type=int, help='max image size')
    parser.add_argument(
        '--ckpt', default=None, type=str, help='load from previous checkpoints'
    )
    parser.add_argument(
        '--no_from_rgb_activate',
        action='store_true',
        help='use activate in from_rgb (original implementation)',
    )
    parser.add_argument(
        '--mixing', action='store_true', help='use mixing regularization'
    )
    parser.add_argument(
        '--loss',
        type=str,
        default='wgan-gp',
        choices=['wgan-gp', 'r1'],
        help='class of gan loss',
    )

    args = parser.parse_args()
    code_size = args.code_size

    generator = nn.DataParallel(StyledGenerator(code_size, args=args)).cuda()
    discriminator = nn.DataParallel(
        Discriminator(from_rgb_activate=not args.no_from_rgb_activate)
    ).cuda()
    g_running = StyledGenerator(code_size, args=args).cuda()
    g_running.train(False)

    g_optimizer = optim.Adam(
        generator.module.generator.parameters(), lr=args.lr, betas=(0.0, 0.99)
    )    
    g_optimizer.add_param_group({'params': generator.module.synt_img_style.parameters()})
    g_optimizer.add_param_group({'params': generator.module.synt_img_style_c.parameters()})
    g_optimizer.add_param_group({'params': generator.module.classifier_on_style_features255.parameters()})
    
    d_optimizer = optim.Adam(discriminator.parameters(), lr=args.lr, betas=(0.0, 0.99))

    accumulate(g_running, generator.module, 0)

    used_sample = None
    step = None
    i = None
    this_step_iters=0
    if args.ckpt is not None:
        ckpt = torch.load(args.ckpt)

        generator.module.load_state_dict(ckpt['generator'], strict=True)
        discriminator.module.load_state_dict(ckpt['discriminator'])
        g_running.load_state_dict(ckpt['g_running'], strict=True)
        g_optimizer.load_state_dict(ckpt['g_optimizer'])
        d_optimizer.load_state_dict(ckpt['d_optimizer'])
        used_sample = ckpt['used_sample']
        this_step_iters = ckpt['this_step_iters']
        step = ckpt['step']
        i = ckpt['i']

    transform = transforms.Compose(
        [
            transforms.ToTensor(),
        ]
    )

    dataset = MultiResolutionDataset(args.path_real, transform,
            icons_signs_path=args.icons_signs_path, real_signs_path=args.real_signs_path,
            rtsd_road_path=args.rtsd_road_path, rtsd_road_json=args.rtsd_road_json,
            inpainted_cropped_path=args.inpainted_cropped_path)

    if args.sched:
        args.lr = {128: 0.0015, 256: 0.002, 512: 0.003, 1024: 0.003}
        args.batch = {4: 16, 8: 16, 16: 16, 32: 16, 64: 16, 128: 16, 256: 16}

    else:
        args.lr = {}
        args.batch = {}

    args.gen_sample = {128: (8, 1)}

    args.batch_default = 16

    train(args, dataset, generator, discriminator, used_sample=used_sample, step=step, i=i, this_step_iters=this_step_iters)
