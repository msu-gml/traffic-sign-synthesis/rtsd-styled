import argparse
import math

import torch
from torchvision import utils

from model import StyledGenerator


from dataset import MultiResolutionDataset


def get_item_by_bbox(bbox_info, scene_img, inpainted_crop, sign, sign_mask, context_frac=0.5):
    max_side = max(bbox_info['h'], bbox_info['w'])
    context_size = int(max_side * context_frac) + 1
    context_size_y = context_size + (max_side - bbox_info['h'] + 1) // 2
    context_size_x = context_size + (max_side - bbox_info['w'] + 1) // 2

    scene_min_y = max(0, bbox_info['y'] - context_size_y)
    scene_max_y = min(scene_img.shape[0], bbox_info['y'] + bbox_info['h'] + context_size_y)
    scene_min_x = max(0, bbox_info['x'] - context_size_x)
    scene_max_x = min(scene_img.shape[1], bbox_info['x'] + bbox_info['w'] + context_size_x)

    paste_from_y = context_size_y - (bbox_info['y'] - scene_min_y)
    paste_to_y = scene_max_y - scene_min_y + paste_from_y
    paste_from_x = context_size_x - (bbox_info['x'] - scene_min_x)
    paste_to_x = scene_max_x - scene_min_x + paste_from_x

    sign_y_from = inpainted_crop.shape[0] // 4
    sign_h_fake = sign.shape[0]
    sign_x_from = inpainted_crop.shape[1] // 4
    sign_w_fake = sign.shape[1]
    inpainted_signed = np.zeros((inpainted_crop.shape[0], inpainted_crop.shape[1], 4))
    inpainted_signed[:,:,:3] = inpainted_crop
    
    
    from skimage.filters import gaussian
    sign_mask = gaussian(sign_mask, 2)
    inpainted_signed[sign_y_from:sign_y_from + sign_h_fake, sign_x_from:sign_x_from + sign_w_fake, :3] = sign * sign_mask + inpainted_signed[sign_y_from:sign_y_from + sign_h_fake, sign_x_from:sign_x_from + sign_w_fake, :3] * (1.0 - sign_mask)
    inpainted_signed[sign_y_from:sign_y_from + sign_h_fake, sign_x_from:sign_x_from + sign_w_fake, 3] = sign_mask[:,:,0]

    inpainted_crop = resize(inpainted_crop, (bbox_info['h'] + context_size_y * 2, bbox_info['w'] + context_size_x * 2), mode='constant', anti_aliasing=True).astype(np.float32)
    inpainted_signed = resize(inpainted_signed, (bbox_info['h'] + context_size_y * 2, bbox_info['w'] + context_size_x * 2), mode='constant', anti_aliasing=True).astype(np.float32)
    
    scene_img[bbox_info['y']:bbox_info['y'] + bbox_info['h'], bbox_info['x']:bbox_info['x'] + bbox_info['w']] = inpainted_crop[bbox_info['y'] - scene_min_y + paste_from_y:bbox_info['y'] - scene_min_y + paste_from_y + bbox_info['h'], bbox_info['x'] - scene_min_x + paste_from_x:bbox_info['x'] - scene_min_x + paste_from_x + bbox_info['w']]
    
    inpainted_signed[:,:,3] = np.maximum(inpainted_signed[:,:,3], gaussian(inpainted_signed[:,:,3], 2, multichannel=False))

    scene_img[scene_min_y:scene_max_y, scene_min_x:scene_max_x] = scene_img[scene_min_y:scene_max_y, scene_min_x:scene_max_x] * (1 - inpainted_signed[paste_from_y:paste_to_y, paste_from_x:paste_to_x,3:]) + inpainted_signed[paste_from_y:paste_to_y, paste_from_x:paste_to_x,:3] * inpainted_signed[paste_from_y:paste_to_y, paste_from_x:paste_to_x,3:]

    return (np.clip(scene_img, 0, 1.0) * 255).astype(np.uint8)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--size', type=int, default=64, help='size of the image')
    parser.add_argument('--n_row', type=int, default=8, help='number of rows of sample matrix')
    parser.add_argument('--n_col', type=int, default=2, help='number of columns of sample matrix')
    parser.add_argument('--save_to', type=str, default='./generated_examples/', help='path to checkpoint file')
    parser.add_argument('--path_real', type=str, default='/mydata/kursovaya4/style-based-gan-pytorch/data/cropped-train/', help='path of target dataset')
    parser.add_argument('--ckpt', type=str, help='load from previous checkpoints')
    parser.add_argument('--types', type=str, help='load from previous checkpoints')
    parser.add_argument('--alpha', type=float, default=1.0, help='number of columns of sample matrix')
    parser.add_argument('--code_size', default=512, type=int, help='initial image size')
    parser.add_argument('--examples_cnt', default=1, type=int, help='initial image size')
    parser.add_argument('--create_full_images', action='store_true', help='use lr scheduling')
    parser.add_argument('--icons_signs_path', type=str, default='/mydata/kursovaya4/style-based-gan-pytorch/data/icons/')
    parser.add_argument('--real_signs_path', type=str, default='/mydata/kursovaya4/style-based-gan-pytorch/data/cropped-train/')
    parser.add_argument('--rtsd_road_path', type=str, default='/mydata/kursovaya4/BicycleGAN/data_rtsd/detection_data/')
    parser.add_argument('--rtsd_road_json', type=str, default='/mydata/kursovaya4/BicycleGAN/data_rtsd/train_rtsd.json')
    parser.add_argument('--inpainted_cropped_path', type=str, default='/mydata/kursovaya4/edge-connect/results_better3/')
    parser.add_argument('--output_dir', type=str, default='./generated_examples/')
    
    args = parser.parse_args()        
    
    from torchvision import datasets, transforms
    transform = transforms.Compose(
        [
            transforms.ToTensor(),
        ]
    )
    
    dataset = MultiResolutionDataset(args.path_real, transform, is_for_train=False, resolution=128,
            icons_signs_path=args.icons_signs_path, real_signs_path=args.real_signs_path,
            rtsd_road_path=args.rtsd_road_path, rtsd_road_json=args.rtsd_road_json,
            inpainted_cropped_path=args.inpainted_cropped_path)
    dataset.resolution = args.size
    import random
    rnd = random.Random(52)
    import os
    all_ids = []
    rear_ids = []
    often_ids = []
    for idx, elem in enumerate(sorted(os.listdir(args.real_signs_path))):
        all_ids.append(idx)
        if len(os.listdir(os.path.join(args.real_signs_path, elem))) > 0:
            often_ids.append(idx)
        else:
            rear_ids.append(idx)
    if args.types == 'all':
        classes = [all_ids[rnd.randint(0, len(all_ids) - 1)] for _ in range(args.n_row * args.n_col)]
    elif args.types == 'rear':
        classes = [rear_ids[rnd.randint(0, len(rear_ids) - 1)] for _ in range(args.n_row * args.n_col)]
    elif args.types == 'often':
        classes = [often_ids[rnd.randint(0, len(often_ids) - 1)] for _ in range(args.n_row * args.n_col)]
    print(len(all_ids), len(rear_ids), len(often_ids))

    import matplotlib.pyplot as plt
    fig, ax = plt.subplots(nrows=args.n_row, ncols=args.n_col * (1 + args.examples_cnt * 2), sharex='col', sharey='row',
                        gridspec_kw={'hspace': 0, 'wspace': 0}, figsize=((1 + args.examples_cnt * 2) * args.n_col / args.n_row * max(1, args.size // 4), max(1, args.size // 4)))

    
    device = 'cuda'
    generator = StyledGenerator(args.code_size, is_mnist=False).to(device)
    generator.load_state_dict(torch.load(args.ckpt)['g_running'], strict=True)
    generator.eval()
    step = int(math.log(args.size, 2)) - 2
    
    for ex_id in range(args.examples_cnt):
        indexes = [rnd.randint(0, 300000) for _ in range(args.n_row * args.n_col)]
        icons = []
        synt_imgs = []
        inpainted_to_encodes = []
        inpainteds = []
        sign_synt_masks = []
        pasted_synts = []
        bbox_infos = []
        for cls, idx in zip(classes, indexes):
            #icon, _, synt, _, _ = dataset.__getitem__(idx)
            icon, synt, inpainted_to_encode, inpainted, sign_synt_mask, pasted_synt, bbox_info = dataset.get_for_test(cls, idx)
            icons.append(icon)
            synt_imgs.append(synt)
            inpainted_to_encodes.append(inpainted_to_encode)
            inpainteds.append(inpainted)
            sign_synt_masks.append(sign_synt_mask)
            pasted_synts.append(pasted_synt)
            bbox_infos.append(bbox_info)

        synt_imgs = torch.stack(synt_imgs).to(device)
        inpainted_to_encodes = torch.stack(inpainted_to_encodes).to(device)
        inpainteds_saved = torch.stack(inpainteds).to(device)
        inpainteds = torch.stack(inpainteds).to(device)
        sign_synt_masks = torch.stack(sign_synt_masks).to(device)
        pasted_synts = torch.stack(pasted_synts).to(device)
            
        import numpy as np
        imgs_generated_torch = generator(synt_imgs, torch.randn(len(synt_imgs), args.code_size).to(device), inpainted_to_encodes, step=step, alpha=args.alpha)[0].to(device)
        
        y_from = inpainteds.shape[2] // 4
        h_fake = imgs_generated_torch.shape[2]
        x_from = inpainteds.shape[3] // 4
        w_fake = imgs_generated_torch.shape[3]
        inpainteds[:,:,y_from:y_from + h_fake, x_from:x_from + w_fake] = imgs_generated_torch * sign_synt_masks + inpainteds[:,:,y_from:y_from + h_fake, x_from:x_from + w_fake] * (1.0 - sign_synt_masks)
        
        imgs_generated = np.array(imgs_generated_torch.data.cpu())
        inpainteds_generated = np.array(inpainteds.data.cpu())
        pasted_synts = np.array(pasted_synts.data.cpu())
        inpainteds_saveds = np.array(inpainteds_saved.data.cpu())
        sign_synt_masks_arr = np.array(sign_synt_masks.data.cpu())
        
        if args.create_full_images:
            for i in range(len(bbox_infos)):
                bbox_info = bbox_infos[i]
                fname = os.path.join(args.rtsd_road_path, bbox_info["fname"])
                from skimage.io import imread, imsave
                from skimage.transform import resize
                scene_img = imread(fname)
                if scene_img.max() > 2.0:
                    scene_img = scene_img / 255.0
                inpainted_crop = np.clip(inpainteds_saveds[i].transpose((1, 2, 0)), 0.0, 1.0)
                sign = np.clip(imgs_generated[i].transpose((1, 2, 0)), 0.0, 1.0)
                sign_mask = np.clip(sign_synt_masks_arr[i].transpose((1, 2, 0)), 0.0, 1.0)
                transformed_img = get_item_by_bbox(bbox_info, scene_img, inpainted_crop, sign, sign_mask)
                fname_synt = os.path.join(args.output_dir, bbox_info["fname"] + ".png")
                imsave(fname_synt, transformed_img)
        

        idd = 0
        for idx in range(args.n_row):    
            for jdx in range(args.n_col):
                ax[idx, jdx * (1 + args.examples_cnt * 2) + 0].imshow(np.array(icons[idd].data.cpu()).transpose((1, 2, 0)))
                
                pasted = np.clip(pasted_synts[idd].transpose((1, 2, 0)), 0.0, 1.0)
                from skimage.transform import resize
                pasted = resize(pasted, inpainteds_generated[idd].shape[1:], mode='constant', anti_aliasing=True).astype(np.float32)

                ax[idx, jdx * (1 + args.examples_cnt * 2) + 1 + ex_id * 2 + 0].imshow(pasted)
                ax[idx, jdx * (1 + args.examples_cnt * 2) + 1 + ex_id * 2 + 1].imshow(np.clip(inpainteds_generated[idd].transpose((1, 2, 0)), 0.0, 1.0))
                ax[idx, jdx * (1 + args.examples_cnt * 2) + 0].set_axis_off()
                ax[idx, jdx * (1 + args.examples_cnt * 2) + 1 + ex_id * 2 + 0].set_axis_off()
                ax[idx, jdx * (1 + args.examples_cnt * 2) + 1 + ex_id * 2 + 1].set_axis_off()
                idd += 1
    import os
    try:
        os.mkdir(args.save_to)
    except:
        a=1
    print(args.save_to + '/' + args.types + args.ckpt.split('/')[-1].split('.')[0] + '.png', args.ckpt.split('/')[-1].split('.')[0])
    fig.savefig(args.save_to + '/' + args.types + args.ckpt.split('/')[-1].split('.')[0] + '.png', bbox_inches = 'tight', pad_inches = 0)
