import argparse
import math

import torch
from torchvision import utils
import numpy as np
from skimage.io import imread, imsave
from skimage.transform import resize

from model import StyledGenerator


from dataset import PicklableMultiResolutionDataset

def my_func(dataset, file_from, file_to, classes):
    synt_imgs = []
    inpainted_to_encodes = []
    inpainteds = []
    sign_synt_masks = []
    bbox_infos = []
    file_groups = []
    cur_cls = []
    futures = []

    batch_pos = 0
    for file_id in range(file_from, file_to):
        group_start_pos = batch_pos
        for idx in dataset.rtsd_dataset.rtsd_road.filenames_to_idxs[file_id][1]:
            futures.append(dataset.get_for_test(classes[idx], idx))
            cur_cls.append(classes[idx])
            batch_pos += 1
        file_groups.append((group_start_pos, batch_pos))
    for res in futures:
        synt, inpainted_to_encode, inpainted, sign_synt_mask, _, bbox_info = res
        synt_imgs.append(synt)
        inpainted_to_encodes.append(inpainted_to_encode)
        inpainteds.append(inpainted)
        sign_synt_masks.append(sign_synt_mask)
        bbox_infos.append(bbox_info)
    return synt_imgs, inpainted_to_encodes, inpainteds, sign_synt_masks, bbox_infos, file_groups, cur_cls

def get_item_by_bbox(bbox_info, scene_img, inpainted_crop, sign, sign_mask, context_frac=0.5):
    max_side = max(bbox_info['h'], bbox_info['w'])
    context_size = int(max_side * context_frac) + 1
    context_size_y = context_size + (max_side - bbox_info['h'] + 1) // 2
    context_size_x = context_size + (max_side - bbox_info['w'] + 1) // 2

    scene_min_y = max(0, bbox_info['y'] - context_size_y)
    scene_max_y = min(scene_img.shape[0], bbox_info['y'] + bbox_info['h'] + context_size_y)
    scene_min_x = max(0, bbox_info['x'] - context_size_x)
    scene_max_x = min(scene_img.shape[1], bbox_info['x'] + bbox_info['w'] + context_size_x)

    paste_from_y = context_size_y - (bbox_info['y'] - scene_min_y)
    paste_to_y = scene_max_y - scene_min_y + paste_from_y
    paste_from_x = context_size_x - (bbox_info['x'] - scene_min_x)
    paste_to_x = scene_max_x - scene_min_x + paste_from_x

    sign_y_from = inpainted_crop.shape[0] // 4
    sign_h_fake = sign.shape[0]
    sign_x_from = inpainted_crop.shape[1] // 4
    sign_w_fake = sign.shape[1]
    inpainted_signed = np.zeros((inpainted_crop.shape[0], inpainted_crop.shape[1], 4))
    inpainted_signed[:,:,:3] = inpainted_crop
    
    
    from skimage.filters import gaussian
    inpainted_signed[sign_y_from:sign_y_from + sign_h_fake, sign_x_from:sign_x_from + sign_w_fake, :3] = sign * sign_mask + inpainted_signed[sign_y_from:sign_y_from + sign_h_fake, sign_x_from:sign_x_from + sign_w_fake, :3] * (1.0 - sign_mask)
    inpainted_signed[sign_y_from:sign_y_from + sign_h_fake, sign_x_from:sign_x_from + sign_w_fake, 3] = sign_mask[:,:,0]

    inpainted_crop = resize(inpainted_crop, (bbox_info['h'] + context_size_y * 2, bbox_info['w'] + context_size_x * 2), mode='constant', anti_aliasing=True).astype(np.float32)
    inpainted_signed = resize(inpainted_signed, (bbox_info['h'] + context_size_y * 2, bbox_info['w'] + context_size_x * 2), mode='constant', anti_aliasing=True).astype(np.float32)
    
    crop_mask_1 = np.zeros((scene_img.shape[0], scene_img.shape[1], 1))
    crop_mask_1[max(0, bbox_info['y'] - 2):min(scene_img.shape[0], bbox_info['y'] + bbox_info['h'] + 2), max(0, bbox_info['x'] - 2):min(scene_img.shape[1], bbox_info['x'] + bbox_info['w'] + 2)] = 1
    crop_mask_1 = gaussian(crop_mask_1, 2)
    crop_mask_1 = crop_mask_1[bbox_info['y']:bbox_info['y'] + bbox_info['h'], bbox_info['x']:bbox_info['x'] + bbox_info['w']]
    
    scene_img[bbox_info['y']:bbox_info['y'] + bbox_info['h'], bbox_info['x']:bbox_info['x'] + bbox_info['w']] = inpainted_crop[bbox_info['y'] - scene_min_y + paste_from_y:bbox_info['y'] - scene_min_y + paste_from_y + bbox_info['h'], bbox_info['x'] - scene_min_x + paste_from_x:bbox_info['x'] - scene_min_x + paste_from_x + bbox_info['w']] * crop_mask_1 + scene_img[bbox_info['y']:bbox_info['y'] + bbox_info['h'], bbox_info['x']:bbox_info['x'] + bbox_info['w']] * (1 - crop_mask_1)
    
    
    from math import sqrt
    min_y = np.argwhere(inpainted_signed[:,:,3].sum(axis=1))[0][0] - paste_from_y + scene_min_y
    max_y = np.argwhere(inpainted_signed[:,:,3].sum(axis=1))[-1][0] - paste_from_y + scene_min_y
    min_x = np.argwhere(inpainted_signed[:,:,3].sum(axis=0))[0][0] - paste_from_x + scene_min_x
    max_x = np.argwhere(inpainted_signed[:,:,3].sum(axis=0))[-1][0] - paste_from_x + scene_min_x
    dy = int((max_y - min_y) * (1 - 1 / sqrt(2)) // 2)
    dx = int((max_x - min_x) * (1 - 1 / sqrt(2)) // 2)
    new_bbox = {
        "y" : int(min_y + dy),
        "x" : int(min_x + dx),
        "h" : int(max_y - min_y - 2 * dy),
        "w" : int(max_x - min_x - 2 * dx),
        "ignore" : False,
    }
    
    inpainted_signed[:,:,3] = np.maximum(inpainted_signed[:,:,3], gaussian(inpainted_signed[:,:,3], 2, multichannel=False))

    data_to_sign = (scene_min_y, scene_max_y, scene_min_x, scene_max_x, paste_from_y, paste_to_y, paste_from_x, paste_to_x, inpainted_signed)
    return scene_img, new_bbox, data_to_sign

def add_sign(scene_img, data_to_sign):
    scene_min_y, scene_max_y, scene_min_x, scene_max_x, paste_from_y, paste_to_y, paste_from_x, paste_to_x, inpainted_signed = data_to_sign
    scene_img[scene_min_y:scene_max_y, scene_min_x:scene_max_x] = scene_img[scene_min_y:scene_max_y, scene_min_x:scene_max_x] * (1 - inpainted_signed[paste_from_y:paste_to_y, paste_from_x:paste_to_x,3:]) + inpainted_signed[paste_from_y:paste_to_y, paste_from_x:paste_to_x,:3] * inpainted_signed[paste_from_y:paste_to_y, paste_from_x:paste_to_x,3:]
    return scene_img




def process_file(file_groups, bbox_infos, inpainteds_saveds, imgs_generated, sign_synt_masks, icons_dataset, cur_cls, rtsd_road_path, output_dir):
    cur_filenames = []
    cur_bboxes_lists = []
    for file_group in file_groups:
        cur_bboxes_list = []
        cur_filename = ""
        scene_img = []
        sign_datas = []
        for i in range(file_group[0], file_group[1]):
            bbox_info = bbox_infos[i]
            fname = os.path.join(rtsd_road_path, bbox_info["fname"])
            if i == file_group[0]:
                scene_img = imread(fname)
                if scene_img.max() > 2.0:
                    scene_img = scene_img / 255.0
                cur_filename = "inp_" + bbox_info["fname"][:-4] + ".png"
            inpainted_crop = np.clip(inpainteds_saveds[i].transpose((1, 2, 0)), 0.0, 1.0)
            sign = np.clip(imgs_generated[i].transpose((1, 2, 0)), 0.0, 1.0)
            sign_mask = np.clip(sign_synt_masks[i].transpose((1, 2, 0)), 0.0, 1.0)
            scene_img, new_bbox, data_to_sign = get_item_by_bbox(bbox_info, scene_img, inpainted_crop, sign, sign_mask)
            new_bbox['sign_class'] = '_'.join(icons_dataset.get_int_to_classes(cur_cls[i]).split('.'))
            cur_bboxes_list.append(new_bbox)
            sign_datas.append(data_to_sign)
        for data_to_sign in sign_datas:
            scene_img = add_sign(scene_img, data_to_sign)
        fname_synt = os.path.join(output_dir, cur_filename)
        scene_img = (np.clip(scene_img, 0, 1.0) * 255).astype(np.uint8)
        imsave(fname_synt, scene_img)
        cur_filenames.append(cur_filename)
        cur_bboxes_lists.append(cur_bboxes_list)
    return cur_filenames, cur_bboxes_lists




if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--size', type=int, default=64, help='size of the image')
    parser.add_argument('--path_real', type=str, default='/mydata/kursovaya4/style-based-gan-pytorch/data/cropped-train/', help='path of target dataset')
    parser.add_argument('--ckpt', type=str, help='load from previous checkpoints')
    parser.add_argument('--types', type=str, help='load from previous checkpoints')
    parser.add_argument('--alpha', type=float, default=1.0, help='number of columns of sample matrix')
    parser.add_argument('--code_size', default=512, type=int, help='initial image size')
    parser.add_argument('--create_full_images', action='store_true', help='use lr scheduling')
    parser.add_argument('--icons_signs_path', type=str, default='/mydata/kursovaya4/style-based-gan-pytorch/data/icons/')
    parser.add_argument('--real_signs_path', type=str, default='/mydata/kursovaya4/style-based-gan-pytorch/data/cropped-train/')
    parser.add_argument('--rtsd_road_path', type=str, default='/mydata/kursovaya4/BicycleGAN/data_rtsd/detection_data/')
    parser.add_argument('--rtsd_road_json', type=str, default='/mydata/kursovaya4/BicycleGAN/data_rtsd/train_rtsd.json')
    parser.add_argument('--inpainted_cropped_path', type=str, default='/mydata/kursovaya4/edge-connect/results_better3/')
    parser.add_argument('--output_dir', type=str, default='./new_images/')
    parser.add_argument('--output_json', type=str, default='new_data.json')
    
    args = parser.parse_args()        
    
    from torchvision import datasets, transforms
    transform = transforms.Compose(
        [
            transforms.ToTensor(),
        ]
    )
    
    dataset = PicklableMultiResolutionDataset(args.path_real, transform, is_for_train=False, resolution=128,
            icons_signs_path=args.icons_signs_path, real_signs_path=args.real_signs_path,
            rtsd_road_path=args.rtsd_road_path, rtsd_road_json=args.rtsd_road_json,
            inpainted_cropped_path=args.inpainted_cropped_path)
    
    all_examples_cnt = len(dataset.rtsd_dataset.inpainted_road.imgs_fnames)
    all_files_cnt = len(dataset.rtsd_dataset.rtsd_road.filenames_to_idxs)
    
    dataset.resolution = args.size
    import random
    rnd = random.Random(52)
    import os
    all_ids = []
    rear_ids = []
    often_ids = []
    for idx, elem in enumerate(sorted(os.listdir(args.path_real))):
        all_ids.append(idx)
        if len(os.listdir(os.path.join(args.path_real, elem))) > 0:
            often_ids.append(idx)
        else:
            rear_ids.append(idx)
    if args.types == 'all':
        classes = [all_ids[rnd.randint(0, len(all_ids) - 1)] for _ in range(all_examples_cnt)]
    elif args.types == 'rear':
        classes = [rear_ids[rnd.randint(0, len(rear_ids) - 1)] for _ in range(all_examples_cnt)]
    elif args.types == 'often':
        classes = [often_ids[rnd.randint(0, len(often_ids) - 1)] for _ in range(all_examples_cnt)]
    print(len(all_ids), len(rear_ids), len(often_ids))

    
    device = 'cuda'
    generator = StyledGenerator(args.code_size, is_mnist=False).to(device)
    generator.load_state_dict(torch.load(args.ckpt)['g_running'], strict=True)
    generator.eval()
    step = int(math.log(args.size, 2)) - 2
    
    batch_size=1
    new_json = {}
    futures_writing = []
    
    threads_cnt = 1
    import concurrent.futures
    with concurrent.futures.ProcessPoolExecutor(max_workers=2 * threads_cnt) as executor:
        for start_file_id in range(0, all_files_cnt, threads_cnt * batch_size):
            futures = []
            for file_id in range(start_file_id, min(start_file_id + threads_cnt * batch_size, all_files_cnt), batch_size):
                futures.append(executor.submit(my_func, dataset, file_id, min(file_id + batch_size, all_files_cnt), classes))

            for future in futures_writing:
                cur_filenames, cur_bboxes_lists = future.result()
                for cur_filename, cur_bboxes_list in zip(cur_filenames, cur_bboxes_lists):
                    new_json[cur_filename] = cur_bboxes_list
            futures_writing = []
            with open(args.output_json, "w") as fp:
                import json
                json.dump(new_json, fp)

            print(start_file_id)
            for future in futures:
                import gc
                gc.collect()
                synt_imgs, inpainted_to_encodes, inpainteds, sign_synt_masks, bbox_infos, file_groups, cur_cls = future.result()

                synt_imgs = torch.tensor(synt_imgs).to(device)
                inpainted_to_encodes = torch.tensor(inpainted_to_encodes).to(device)
                inpainteds_saveds = np.array(inpainteds)
                sign_synt_masks = np.array(sign_synt_masks)
                inpainteds = np.array(inpainteds)

                imgs_generated_torch = generator(synt_imgs, torch.randn(len(synt_imgs), args.code_size).to(device), inpainted_to_encodes, step=step, alpha=args.alpha, is_inference=True).data.cpu()
                imgs_generated = np.array(imgs_generated_torch)
                synt_imgs = synt_imgs.data.cpu()
                inpainted_to_encodes = inpainted_to_encodes.data.cpu()
                del inpainted_to_encodes, synt_imgs, imgs_generated_torch

                
                y_from = inpainteds.shape[2] // 4
                h_fake = imgs_generated.shape[2]
                x_from = inpainteds.shape[3] // 4
                w_fake = imgs_generated.shape[3]
                inpainteds[:,:,y_from:y_from + h_fake, x_from:x_from + w_fake] = imgs_generated * sign_synt_masks + inpainteds[:,:,y_from:y_from + h_fake, x_from:x_from + w_fake] * (1.0 - sign_synt_masks)
                
                futures_writing.append(executor.submit(process_file, file_groups, bbox_infos, inpainteds_saveds, imgs_generated, sign_synt_masks, dataset.rtsd_dataset.icons_dataset, cur_cls, args.rtsd_road_path, args.output_dir))
    
        for future in futures_writing:
            cur_filenames, cur_bboxes_lists = future.result()
            for cur_filename, cur_bboxes_list in zip(cur_filenames, cur_bboxes_lists):
                new_json[cur_filename] = cur_bboxes_list
        futures_writing = []

    with open(args.output_json, "w") as fp:
        import json
        json.dump(new_json, fp)