Train network (80k iterations enough):
```
python train.py --vgg_loss --cls_loss
```

Evaluate on small amount of examples:
```
python generate.py --ckpt='./checkpoint7/087000.model' --size=64 --alpha=1.0 --types='rear' --code_size=612 --examples_cnt=3
```

Generate data:
```
generate_dataset.py
```
